({
	bindVariableTwo : function(component, event) {
        var event = $A.get("e.c:DemoBindEvent");
        var bindtwo = component.get("v.bindtwo");
        event.setParams({"newValue": bindtwo});
        event.fire();
	}
})