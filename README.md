# Bluewolf Tech Meeting - Lightning Demo

This demonstration was put together to show the different ways variables can be moved between parent-child or sibling componets; binding and events.

## Getting Started

You can either create the component one by one by copying and pasting the code or you can add the Aura folder into the src of an ANT package and upload it that way.

### Prerequisites

None

## Authors

* **David Dawson** - [Bluewolf](bluewolf.com/people/david-dawson)

## License

For internal use only
